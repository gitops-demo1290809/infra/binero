# Configure binero openstack
terraform {
  required_providers {
    openstack = {
      source  = "terraform-provider-openstack/openstack"
      version = "1.48.0"
    }
  }
}

# Configure the OpenStack Provider
provider "openstack" {
  region        = "europe-se-1"
  endpoint_type = "public"
}