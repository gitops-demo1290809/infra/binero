data "openstack_compute_flavor_v2" "demoflavor" {
  name = "gp.1x2"
}

data "openstack_images_image_v2" "centos8" {
  name = "centos-stream-8-x86_64"
}

data "openstack_networking_network_v2" "network" {
  name = "europe-se-1-1a-net0"
}

# Create Key Pair
# resource "openstack_compute_keypair_v2" "keypair" {
#   name       = "openshift-keypair"
#   public_key = file("~/.ssh/id_rsa.pub")
# }

# # Create the OpenShift 4.11 Cluster
# resource "openstack_compute_instance_v2" "master" {
#   name            = "master"
#   image_name      = data.openstack_images_image_v2.centos8.name
#   flavor_name     = data.openstack_compute_flavor_v2.demoflavor.name
#   key_pair        = openstack_compute_keypair_v2.keypair.name
#   security_groups = [openstack_networking_secgroup_v2.secgroup.name]
#   network {
#     name = data.openstack_networking_network_v2.network.name
#   }
#   user_data = <<EOF
# #!/bin/bash
# # Install OpenShift 4.11
# curl -s https://mirror.openshift.com/pub/openshift-v4/clients/ocp/latest/openshift-install-linux.tar.gz | tar -xz
# ./openshift-install create cluster --dir=./openshift-install --log-level=debug
# EOF
# }

